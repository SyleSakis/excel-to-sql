/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exceltosql;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.DataFormatter;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author SakaroS
 */
public class ExceltoSQL {
    
    private static final ArrayList<String> stringHeader = new ArrayList<>(Arrays.asList("A","B","C","D","E","H","J","M","O","P",
            "Q","S","T","U","W","X","Z","AA","AB","AF","AJ","AL","AO","AR","AV","AW","AX","AY","AZ","BA","BB","BD","BE","BH","BI"));
    private static final ArrayList<String> floatHeader = new ArrayList<>(Arrays.asList("G","I","K","AM","AN","AP","AQ","AS","AT"));
    private static final ArrayList<String> intHeader = new ArrayList<>(Arrays.asList("AD","AH","AK","BG"));

    private static final ArrayList<String> newGamesHeader = new ArrayList<>(Arrays.asList("A","B","D","C","E","H","J","G","I","K",
            "AM","AN","AP","AQ","AS","AT"));
    private static final ArrayList<String> newGamesStringHeader = new ArrayList<>(Arrays.asList("A","B","D","C","E","H","J"));
    private static final ArrayList<String> newGamesFloatHeader = new ArrayList<>(Arrays.asList("G","I","K","AM","AN","AP","AQ","AS","AT"));
    
    
    private static final SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
    
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
    
    private static final int B = 1; private static final int D = 3; private static final int BI = 60;  // "B" "D" -> dates  "BI" -> String
    private static final int AT = 45; // end of string in new games
    
    private static final int final_score = 14;  // final_score "O"

    public static void OldGamesReadXLSFile(InputStream ExcelFileToRead, String path, String name) throws IOException {
        ArrayList<Integer> stringIntHeader = new ArrayList<Integer>();
        stringIntHeader.addAll(convertListStringtoInt(ExceltoSQL.stringHeader));
        
        ArrayList<Integer> floatIntHeader = new ArrayList<Integer>();
        floatIntHeader.addAll(convertListStringtoInt(ExceltoSQL.floatHeader));
        
        ArrayList<Integer> intIntHeader = new ArrayList<Integer>();
        intIntHeader.addAll(convertListStringtoInt(ExceltoSQL.intHeader));
        
        ArrayList<Integer> IntHeader = new ArrayList<Integer>();
        IntHeader.addAll(MergeandShortArrays(stringHeader,floatHeader,intHeader));
        
        DataFormatter formatter = new DataFormatter(true);
        
        String sql = new String();
        HSSFWorkbook wb = new HSSFWorkbook(ExcelFileToRead);

        //HSSFSheet sheet = wb.getSheetAt(0);
        HSSFSheet sheet = wb.getSheetAt(wb.getActiveSheetIndex());
        HSSFRow row;
        HSSFCell cell = null;

        Iterator rows = sheet.rowIterator();
       
        row = (HSSFRow) rows.next();
        
        sql = SQLHeader();
        //System.out.println("String: "+stringIntHeader);
       // System.out.println("float: "+floatIntHeader);
        //System.out.println("int: "+intIntHeader);

        for(int j=1; j<=sheet.getLastRowNum(); j++){
            row = sheet.getRow(j);
            if(row != null){
                cell = sheet.getRow(j).getCell(final_score);  // if the games dosent have final score skip it
                if(cell.toString() != ""){
                    if(j>1 && j<=sheet.getLastRowNum()){
                        sql+=","+"\n";
                    }
                    sql+="(";
                    for(int i : IntHeader){
                        cell = sheet.getRow(j).getCell(i);
                        String cellstring = formatter.formatCellValue(cell);
                        try {
                            if(MyContains(stringIntHeader,i)){
                                if(i == B){
                                  //  System.out.println("DATE: "+dateFormat.format(cell.getDateCellValue()));
                                    //sql+="B-> ";
                                    sql+="'"+dateFormat.format(cell.getDateCellValue())+"',";
                                }else if(i == D){
                                    //System.out.println("TIME: "+timeFormat.format(cell.getDateCellValue()));
                                    //sql+="D-> ";
                                    sql+="'"+timeFormat.format(cell.getDateCellValue())+"',";
                                }else{
                                    cell.setCellType(HSSFCell.CELL_TYPE_STRING);
                                    String cellvalue = (String)cell.getStringCellValue().replaceAll("'", ""); 
                                    if(i != BI){
                                        //sql+="NOT BI i-> "+ i+"   ";
                                        sql+="'"+cellvalue+"',";
                                    }else{
                                        //sql+="BI i-> "+ i+"   ";
                                        sql+="'"+cellvalue+"'";
                                    }
                                }
                            }else if(MyContains(floatIntHeader,i)){
                                if(cell == null){
                                    sql+="'"+0.00+"',";
                                }else{
                                    String value = String.format("%.2f",cell.getNumericCellValue()).replace(",", ".");
                                    //sql+="FLOAT i-> "+ i+"   ";
                                    sql+="'"+value+"',";
                                }
                            }else if(MyContains(intIntHeader,i)){
                                if(cell == null){
                                    sql+="'"+0+"'";
                                }else{
                                    int intcell = (int)cell.getNumericCellValue();
                                    //sql+="INTEGER i-> "+ i+"   ";
                                    sql+="'"+intcell+"',";
                                }
                            }
                        }catch(NullPointerException e){
                             if(MyContains(stringIntHeader,i)){
                                if(i == B){
                                    Date date = Date.from(Instant.now());
                                   // sql+="Bnull i-> "+ i+"   ";
                                    sql+="'"+dateFormat.format(date)+"',";
                                }else if(i == D){
                                   // sql+="Dnull i-> "+ i+"   ";
                                    Date date = Date.from(Instant.now());
                                    sql+="'"+timeFormat.format(date)+"',";
                                }else{
                                    if(i != BI){
                                     //   sql+="NOT BInull i-> "+ i+"   ";
                                        sql+="'"+"',";
                                    }else{
                                      //  sql+="BInull i-> "+ i+"   ";
                                        sql+="'"+"'";
                                    }
                                }
                            }else if(MyContains(floatIntHeader,i)){
                                //sql+="FLOATnull i-> "+ i+"   ";
                                sql+="'"+0.00+"',";
                            }else if(MyContains(intIntHeader,i)){
                               // sql+="Integernull i-> "+ i+"   ";
                                sql+="'"+0+"',";
                            }                         
                        }
                    }
                    sql+=")";
                }
                //System.out.println();  
            }
        }
        sql+=";";
        writeTOSQLFile(sql,path,name);
    }

    public static void OldGamesReadXLSXFile(InputStream ExcelFileToRead, String path, String name) throws IOException{
        ArrayList<Integer> stringIntHeader = new ArrayList<Integer>();
        stringIntHeader.addAll(convertListStringtoInt(ExceltoSQL.stringHeader));
        
        ArrayList<Integer> floatIntHeader = new ArrayList<Integer>();
        floatIntHeader.addAll(convertListStringtoInt(ExceltoSQL.floatHeader));
        
        ArrayList<Integer> intIntHeader = new ArrayList<Integer>();
        intIntHeader.addAll(convertListStringtoInt(ExceltoSQL.intHeader));
        
        ArrayList<Integer> IntHeader = new ArrayList<Integer>();
        IntHeader.addAll(MergeandShortArrays(stringHeader,floatHeader,intHeader));
        
        String sql = new String();
        XSSFWorkbook wb = new XSSFWorkbook(ExcelFileToRead);

        //XSSFSheet sheet = wb.getSheetAt(0);
        XSSFSheet sheet = wb.getSheetAt(wb.getActiveSheetIndex());
        XSSFRow row;
        XSSFCell cell = null;

        Iterator rows = sheet.rowIterator();
       
        row = (XSSFRow) rows.next();
        
        sql = SQLHeader();
        //System.out.println("String: "+stringIntHeader);
        //System.out.println("float: "+floatIntHeader);
        //System.out.println("int: "+intIntHeader);

        for(int j=1; j<=sheet.getLastRowNum(); j++){
            row = sheet.getRow(j);
            if(row != null){
                cell = sheet.getRow(j).getCell(final_score);  // if the games dosent have final score skip it
                if(cell.toString() != ""){
                    if(j>1 && j<=sheet.getLastRowNum()){
                        sql+=","+"\n";
                    }
                    sql+="(";
                    for(int i : IntHeader){
                        cell = sheet.getRow(j).getCell(i);
                        try {
                            if(MyContains(stringIntHeader,i)){
                                if(i == B){
                                    //System.out.println("DATE: "+dateFormat.format(cell.getDateCellValue()));
                                  //  sql+="B-> ";
                                    sql+="'"+dateFormat.format(cell.getDateCellValue())+"',";
                                }else if(i == D){
                                   // System.out.println("TIME: "+timeFormat.format(cell.getDateCellValue()));
                                 //   sql+="D-> ";
                                    sql+="'"+timeFormat.format(cell.getDateCellValue())+"',";
                                }else{
                                    cell.setCellType(XSSFCell.CELL_TYPE_STRING);
                                    String cellvalue = (String)cell.getStringCellValue().replaceAll("'", ""); 
                                    if(i != BI){
                                     //   sql+="NOT BI i-> "+ i+"   ";
                                        sql+="'"+cellvalue+"',";
                                    }else{
                                    //    sql+="BI i-> "+ i+"   ";
                                        sql+="'"+cellvalue+"'";
                                    }
                                }
                            }else if(MyContains(floatIntHeader,i)){
                                if(cell == null){
                                    sql+="'"+0.00+"',";
                                }else{
                                    //double value = Double.parseDouble(new DecimalFormat("#.##").format(cell.getNumericCellValue()));
                                    String value = String.format("%.2f",cell.getNumericCellValue()).replace(",", ".");
                                   // sql+="FLOAT i-> "+ i+"   ";
                                    sql+="'"+value+"',";
                                }
                            }else if(MyContains(intIntHeader,i)){
                                if(cell == null){
                                    sql+="'"+0+"'";
                                }else{
                                    int intcell = (int)cell.getNumericCellValue();
                                    //sql+="INTEGER i-> "+ i+"   ";
                                    sql+="'"+intcell+"',";
                                }
                            }
                        }catch(NullPointerException e){
                             if(MyContains(stringIntHeader,i)){
                                if(i == B){
                                    Date date = Date.from(Instant.now());
                                   // sql+="Bnull i-> "+ i+"   ";
                                    sql+="'"+dateFormat.format(date)+"',";
                                }else if(i == D){
                                    //sql+="Dnull i-> "+ i+"   ";
                                    Date date = Date.from(Instant.now());
                                    sql+="'"+timeFormat.format(date)+"',";
                                }else{
                                    if(i != BI){
                                       // sql+="NOT BInull i-> "+ i+"   ";
                                        sql+="'"+"',";
                                    }else{
                                       // sql+="BInull i-> "+ i+"   ";
                                        sql+="'"+"'";
                                    }
                                }
                            }else if(MyContains(floatIntHeader,i)){
                                //sql+="FLOATnull i-> "+ i+"   ";
                                sql+="'"+0.00+"',";
                            }else if(MyContains(intIntHeader,i)){
                                //sql+="Integernull i-> "+ i+"   ";
                                sql+="'"+0+"',";
                            }                         
                        }
                    }
                    sql+=")";
                }
                //System.out.println();  
            }
        }
        sql+=";";
        writeTOSQLFile(sql,path,name);
    }
    
    public static void NewGamesReadXLSFile(InputStream ExcelFileToRead, String path, String name) throws IOException{
        ArrayList<Integer> newGamesIntHeader = new ArrayList<Integer>();
        newGamesIntHeader.addAll(convertListStringtoInt(ExceltoSQL.newGamesHeader));
        
        ArrayList<Integer> stringIntHeader = new ArrayList<Integer>();
        stringIntHeader.addAll(convertListStringtoInt(ExceltoSQL.newGamesStringHeader));
        
        ArrayList<Integer> floatIntHeader = new ArrayList<Integer>();
        floatIntHeader.addAll(convertListStringtoInt(ExceltoSQL.newGamesFloatHeader));
        
       // System.out.println(newGamesIntHeader);
        
        DataFormatter formatter = new DataFormatter(true);
        
        String sql = new String();
        HSSFWorkbook wb = new HSSFWorkbook(ExcelFileToRead);

        //HSSFSheet sheet = wb.getSheetAt(0);
        HSSFSheet sheet = wb.getSheetAt(wb.getActiveSheetIndex());
        HSSFRow row;
        HSSFCell cell = null;

        Iterator rows = sheet.rowIterator();
       
        row = (HSSFRow) rows.next();
        
        sql = newGamesSQLHeader();
        
        for(int j=1; j<=sheet.getLastRowNum(); j++){
            row = sheet.getRow(j);
            if(row != null){
                cell = sheet.getRow(j).getCell(final_score);  // if the games dosent have final score skip it
                if(cell.toString() == ""){
                    if(j>1 && j<=sheet.getLastRowNum()){
                        sql+=","+"\n";
                    }
                    sql+="(";
                    for(int i : newGamesIntHeader){
                        cell = sheet.getRow(j).getCell(i);
                        String cellstring = formatter.formatCellValue(cell);
                        try {
                            if(MyContains(stringIntHeader,i)){
                                if(i == B){
                                  //  System.out.println("DATE: "+dateFormat.format(cell.getDateCellValue()));
                                    //sql+="B-> ";
                                    sql+="'"+dateFormat.format(cell.getDateCellValue())+"',";
                                }else if(i == D){
                                    //System.out.println("TIME: "+timeFormat.format(cell.getDateCellValue()));
                                    //sql+="D-> ";
                                    sql+="'"+timeFormat.format(cell.getDateCellValue())+"',";
                                }else{
                                    cell.setCellType(HSSFCell.CELL_TYPE_STRING);
                                    String cellvalue = (String)cell.getStringCellValue().replaceAll("'", ""); 
                                    sql+="'"+cellvalue+"',";
                                }
                            }else {
                                if(i != AT){
                                    if(cell == null){
                                        sql+="'"+0.00+"',";
                                    }else{
                                        String value = String.format("%.2f",cell.getNumericCellValue()).replace(",", ".");
                                        //sql+="FLOAT i-> "+ i+"   ";
                                        sql+="'"+value+"',";
                                    }
                                }else{
                                    if(cell == null){
                                        sql+="'"+0.00+"'";
                                    }else{
                                        String value = String.format("%.2f",cell.getNumericCellValue()).replace(",", ".");
                                        //sql+="FLOAT i-> "+ i+"   ";
                                        sql+="'"+value+"'";
                                    }                                    
                                }
                            }
                        }catch(NullPointerException e){
                             if(MyContains(stringIntHeader,i)){
                                if(i == B){
                                    Date date = Date.from(Instant.now());
                                   // sql+="Bnull i-> "+ i+"   ";
                                    sql+="'"+dateFormat.format(date)+"',";
                                }else if(i == D){
                                   // sql+="Dnull i-> "+ i+"   ";
                                    Date date = Date.from(Instant.now());
                                    sql+="'"+timeFormat.format(date)+"',";
                                }else{
                                    sql+="'"+"',";
                                }
                            }else{
                                if(i != AT){
                                    // sql+="Integernull i-> "+ i+"   ";
                                    sql+="'"+0.00+"',";
                                }else{
                                    // sql+="Integernull i-> "+ i+"   ";
                                    sql+="'"+0.00+"'";                                    
                                }
                            }                         
                        }
                    }
                    sql+=")";
                }
                //System.out.println();  
            }
        }
        sql+=";";
        newGameswriteTOSQLFile(sql,path,name);    
    }
    
    public static void NewGamesReadXLSXFile(InputStream ExcelFileToRead, String path, String name) throws IOException{
        ArrayList<Integer> newGamesIntHeader = new ArrayList<Integer>();
        newGamesIntHeader.addAll(convertListStringtoInt(ExceltoSQL.newGamesHeader));
        
        ArrayList<Integer> stringIntHeader = new ArrayList<Integer>();
        stringIntHeader.addAll(convertListStringtoInt(ExceltoSQL.newGamesStringHeader));
        
        ArrayList<Integer> floatIntHeader = new ArrayList<Integer>();
        floatIntHeader.addAll(convertListStringtoInt(ExceltoSQL.newGamesFloatHeader));
        
       // System.out.println(newGamesIntHeader);
        
        DataFormatter formatter = new DataFormatter(true);
        
        String sql = new String();
        XSSFWorkbook wb = new XSSFWorkbook(ExcelFileToRead);

        //XSSFSheet sheet = wb.getSheetAt(0);
        XSSFSheet sheet = wb.getSheetAt(wb.getActiveSheetIndex());
        XSSFRow row;
        XSSFCell cell = null;

        Iterator rows = sheet.rowIterator();
       
        row = (XSSFRow) rows.next();
        
        sql = newGamesSQLHeader();
        
        for(int j=1; j<=sheet.getLastRowNum(); j++){
            row = sheet.getRow(j);
            if(row != null){
                cell = sheet.getRow(j).getCell(final_score);  // if the games dosent have final score skip it
                if(cell.toString() == ""){
                    if(j>1 && j<=sheet.getLastRowNum()){
                        sql+=","+"\n";
                    }
                    sql+="(";
                    for(int i : newGamesIntHeader){
                        cell = sheet.getRow(j).getCell(i);
                        String cellstring = formatter.formatCellValue(cell);
                        try {
                            if(MyContains(stringIntHeader,i)){
                                if(i == B){
                                  //  System.out.println("DATE: "+dateFormat.format(cell.getDateCellValue()));
                                    //sql+="B-> ";
                                    sql+="'"+dateFormat.format(cell.getDateCellValue())+"',";
                                }else if(i == D){
                                    //System.out.println("TIME: "+timeFormat.format(cell.getDateCellValue()));
                                    //sql+="D-> ";
                                    sql+="'"+timeFormat.format(cell.getDateCellValue())+"',";
                                }else{
                                    cell.setCellType(HSSFCell.CELL_TYPE_STRING);
                                    String cellvalue = (String)cell.getStringCellValue().replaceAll("'", ""); 
                                    sql+="'"+cellvalue+"',";
                                }
                            }else {
                                if(i != AT){
                                    if(cell == null){
                                        sql+="'"+0.00+"',";
                                    }else{
                                        String value = String.format("%.2f",cell.getNumericCellValue()).replace(",", ".");
                                        //sql+="FLOAT i-> "+ i+"   ";
                                        sql+="'"+value+"',";
                                    }
                                }else{
                                    if(cell == null){
                                        sql+="'"+0.00+"'";
                                    }else{
                                        String value = String.format("%.2f",cell.getNumericCellValue()).replace(",", ".");
                                        //sql+="FLOAT i-> "+ i+"   ";
                                        sql+="'"+value+"'";
                                    }                                    
                                }
                            }
                        }catch(NullPointerException e){
                             if(MyContains(stringIntHeader,i)){
                                if(i == B){
                                    Date date = Date.from(Instant.now());
                                   // sql+="Bnull i-> "+ i+"   ";
                                    sql+="'"+dateFormat.format(date)+"',";
                                }else if(i == D){
                                   // sql+="Dnull i-> "+ i+"   ";
                                    Date date = Date.from(Instant.now());
                                    sql+="'"+timeFormat.format(date)+"',";
                                }else{
                                    sql+="'"+"',";
                                }
                            }else{
                                if(i != AT){
                                    // sql+="Integernull i-> "+ i+"   ";
                                    sql+="'"+0.00+"',";
                                }else{
                                    // sql+="Integernull i-> "+ i+"   ";
                                    sql+="'"+0.00+"'";                                    
                                }
                            }                         
                        }
                    }
                    sql+=")";
                }
                //System.out.println();  
            }
        }
        sql+=";";
        newGameswriteTOSQLFile(sql,path,name);
    }
    
    private static String SQLHeader() {
        String sql="SET SQL_MODE = \"NO_AUTO_VALUE_ON_ZERO\";"+"\n";
        
        sql += "INSERT INTO betprofit.old_games (year, date, leagueKey, time, OpapKey, homeOdds, homeName, drawOdds, "
                        + "awayName, awayOdds, final, finalScore, sec0goals, hf, homeHalfsWon, halfScore, awayHalfsWon, "
                        + "firstOverUnder15, secOverUnder15, homeWonBoth, half, awayWonBoth, homeMore, sec, awayMore, "
                        + "finalOverUnder15, sunoloGoal, finalOverUnder25, under25odds, over25odds, finalOverUnder35, "
                        + "under35odds, over35odds, goalNoGoal, ggOdds, ngOdds, homeFirstHalfGoals, awayFirstHalfGoals, "
                        + "homeBothGoals, finalGoalGroup, awayBothGoals, homeSecGoals, awaySecGoals, homeOverUnder15, "
                        + "awayOverUnder15, secMore, underdog05, underdog15) VALUES "+"\n";      
        return sql;
    }
    
     private static void writeTOSQLFile(String sql, String path, String name){
        SimpleDateFormat myFormat = new SimpleDateFormat("dd.MM_HH.mm ");
        String SQLFile = myFormat.format(Date.from(Instant.now()))+"oldGames.sql";
        path = path.replace(name, SQLFile);
        Writer outputSQL = null;
        try {
            outputSQL = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(path), "UTF-8"));
            //outputSQL = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(path)));
            outputSQL.write(sql);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ExceltoSQL.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ExceltoSQL.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ExceltoSQL.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                outputSQL.close();
            } catch (IOException ex) {
                Logger.getLogger(ExceltoSQL.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.out.println("DONE!!");
        }

    }
     
    private static String newGamesSQLHeader() {
        String sql="SET SQL_MODE = \"NO_AUTO_VALUE_ON_ZERO\";"+"\n";
        
        sql += "INSERT INTO betprofit.new_games (year, date, time, leagueKey, OpapKey, homeName, awayName, homeOdds, "
                + "drawOdds, awayOdds, under25odds, over25odds, under35odds, over35odds, ggOdds, ngOdds) VALUES"+"\n";
  
        return sql;
    } 
    
    private static void newGameswriteTOSQLFile(String sql, String path, String name){
        SimpleDateFormat myFormat = new SimpleDateFormat("dd.MM_HH.mm ");
        String SQLFile = myFormat.format(Date.from(Instant.now()))+"newGames.sql";
        path = path.replace(name, SQLFile);
        Writer outputSQL = null;
        try {
            outputSQL = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(path), "UTF-8"));
            //outputSQL = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(path)));
            outputSQL.write(sql);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ExceltoSQL.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ExceltoSQL.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ExceltoSQL.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                outputSQL.close();
            } catch (IOException ex) {
                Logger.getLogger(ExceltoSQL.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.out.println("DONE!!");
        }
    }

    private static boolean isCellString(String cellReference) {
        if(stringHeader.contains(cellReference)){
            return true;
        }
        return false;
    }

    private static boolean isCellFloat(String cellReference) {
        if(floatHeader.contains(cellReference)){
            return true;
        }
        return false;
    }

    private static boolean isCellInteger(String cellReference) {
        if(intHeader.contains(cellReference)){
            return true;
        }
        return false;
    }
    
    private static boolean isCell(String reference){
        if(isCellString(reference) || isCellFloat(reference) || isCellInteger(reference)){
            return true;
    }
        return false;
    }

    private static ArrayList convertListStringtoInt(ArrayList<String> arraylist) {
        ArrayList<Integer> list = new ArrayList<Integer>();
        for(String item : arraylist){
            char[] ch = item.toCharArray();
            int value = 0;
            for(int i=0; i<ch.length; i++){
                int c = (int)ch[i];
                if(i!=ch.length-1){
                    value+=(c-64)*26;
                }else{
                    value +=(c-64);
                }
            }
            list.add(value-1);

        }
        //System.out.println("Str : "+arraylist.toString());
        //System.out.println("list: "+list.toString());
        return list;
    }
    
    private static ArrayList<Integer> MergeandShortArrays(ArrayList<String> arraylist1, 
            ArrayList<String> arraylist2, ArrayList<String> arraylist3){
        ArrayList<Integer> list = new ArrayList<>();
        list.addAll(convertListStringtoInt(arraylist1));
        list.addAll(convertListStringtoInt(arraylist2));
        list.addAll(convertListStringtoInt(arraylist3));
        
        Collections.sort(list);
       // System.out.println(list);
        return list;
    }

    private static boolean MyContains(ArrayList<Integer> list, int i) {
        for(int item: list ){
            if(item == i){
                return true;
            }
        }
        return false;
    }

}
